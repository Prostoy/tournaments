<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Match extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'tournament_id',
        'team1',
        'team2',
        'team1_score',
        'team2_score',
        'parent_id'
    ];
    protected $hidden = ['parent_id'];

    /**
     * @param \App\Tournament $tournamentId
     * @param int             $count
     */
    public static function generateMatches(Tournament $tournament, int $untilCount): array
    {
        $parents = [];
        //region Создаем корень (финал)
        DB::beginTransaction();

        $parents[] = self::create(
            [
                'tournament_id' => $tournament->id,
                'parent_id'     => 0
            ]
        );
        //endregion Создаем корень (финал)

        //region Создаем остальные матчи
        while (count($parents) < $untilCount) {
            $newParents = [];
            foreach ($parents as $match) {
                $c = 2;
                while ($c--) {
                    $newParents[] = self::create(
                        [
                            'tournament_id' => $tournament->id,
                            'parent_id'     => $match->id
                        ]
                    );
                }
            }

            $parents = $newParents;
        }
        DB::commit();

        return $parents;
    }
}
