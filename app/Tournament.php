<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tournament extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
    ];

    private const checkCount = [
        2,
        4,
        8,
        16,
        36
    ];

    /**
     * Создание турнира
     *
     * @param array $teams
     */
    public static function build(array $teams, string $name): Tournament
    {
        $tournament = self::create(['name' => $name]);

        $count = count($teams);
        if (in_array($count, self::checkCount)) {
            $untilCount = $count / 2;
            $lifes = Match::generateMatches($tournament, $untilCount);

            $teams = array_chunk($teams, 2);
            foreach ($lifes as $match) {
                $curTeams = array_shift($teams);
                $match->update(array_combine(['team1', 'team2'], $curTeams));
            }
        } else {
            throw new Exception('Количество участников не является степенью двойки');
        }

        return $tournament;
    }
}

